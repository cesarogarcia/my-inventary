import { shallowMount } from '@vue/test-utils'
import HomePage from '@/components/HomePage.vue'
import {mockRouter} from "./helpers"

function mountPage() {return shallowMount(HomePage, {
    mocks:{$router:mockRouter}
})}


test('pinta el botón go-main', () => {
    const wrapper = shallowMount(HomePage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.go-main')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón go-main', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.go-main').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/main')

   
})