import { shallowMount } from '@vue/test-utils'
import ProductItem from '@/components/Products/ProductItem.vue'
import { mockRouter } from './helpers'

function mountPage() {

    return shallowMount(ProductItem, {
        propsData: {
            product: {
                id: '2',
                type: 'entry',
                quanty:'24',
                category: 'papeleria',
                name: 'libro',
                date: '2020-05-27',
            },
        },
        
        mocks: { $router: mockRouter },
    })
}

test('Verifica que saca el nombre debido', () => {
    const wrapper = shallowMount(ProductItem, {
        propsData: {
            product: {
                id: '2',
                type: 'entry',
                quanty:'24',
                category: 'papeleria',
                name: 'libro',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.text()).toContain('libro 24 papeleria entry 2020-05-27')
})


test('ejecuta el botón seeProduct', async () => {
    const wrapper = mountPage()
    wrapper.find('.full-name').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/details/2')

   
})

test('ejecuta el botón editProduct', async () => {
    const wrapper = mountPage()
    wrapper.findAll('#edit').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/details/2')

   
})

test('emite evento click deleteProduct', async () => {
    const wrapper = shallowMount(ProductItem, {
        propsData: {
            product: {
                id: '2',
                type: 'entry',
                quanty:'24',
                category: 'papeleria',
                name: 'libro',
                date: '2020-05-27',
            },
        },
    })
    expect(wrapper.emitted('delete-product-event')).toBe(undefined)

    // Simular lo que hace el usuario.
    const namej = wrapper.find('#delete')
    namej.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('delete-product-event').length).toBe(1)
    expect(wrapper.emitted('delete-product-event')[0]).toEqual(['2'])
})



test('Crear un Computed para meter en fullName la concatenación de name y surname.', async () => {
    const wrapper = shallowMount(ProductItem, {
        propsData: {
            product: {
                id: '2',
                type: 'entry',
                quanty:'24',
                category: 'papeleria',
                name: 'libro',
                date: '2020-05-27',
            },
        },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.fullProduct).toContain('libro 24 papeleria entry 2020-05-27')
})

