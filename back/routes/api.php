<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//consulta de todos los datos(todos los movimientos)
Route::get('/products', function (Request $request) {
    $results = DB::select('select id, type, quanty, date, category, name, observations from products  ORDER BY  date DESC, dateaudit DESC');
    return response()->json($results, 200);
});

//consulta de un producto por nombre
//select * from products WHERE name LIKE "%Sacapuntas%"
Route::get('/search_product/{txt}', function ($txt) {
   $results = DB::select('select * from products WHERE name LIKE "%' . $txt . '%"');
   return response()->json($results, 200);
});

// envio de informacion(movimientos nuevos)
Route::post('/products/', function() {
$data = request()->all();
DB::insert(
    "
    insert into products (id, type, quanty, date, category, name, observations)
    values (:id, :type, :quanty, :date, :category, :name, :observations)
    ",
    $data
);
$results = DB::select('select * from products where id = :id', [
    'id' => $data['id'],
]);
return response()->json($results[0], 200);
});


//Consulta entre fecha

Route::get('/products/bydate', function () {
  
        $data = request()->all();
        $initdate= $data["from"];
        $enddate= $data["to"];

        $results = DB::Select('select * from products WHERE date BETWEEN :initdate AND :enddate',
        [
            'initdate' => $initdate,
            'enddate' => $enddate,
        ]);
             
        return response()->json($results, 200);
    });


    //Consulta Stock TOTAL de Productos
 
Route::get('/products/get_stock/{name}', function ($name) {

    $results = DB::select('select name, sum(E - O) as numberOfItems from 
    (select name, 0 as E, sum(quanty) as O from products where name=:name and type="out"  group by 1 , E
    
    union
    
    select name, sum(quanty) as E, 0 as O from products where name=:name and type="entry"  group by 1 , O)
     as stock group by 1', [
        'name' => $name,
    ]);
    return response()->json($results[0], 200);
});


//consulta de movimiento por id
Route::get('/products/{id}', function ($id) {
   
    if (NoExists($id)) {
        abort(404);
    }
    
    $results = DB::select('select * from products where id=:id ', [
        'id' => $id,
    ]);

    return response()->json($results[0], 200);
});


//borrar movimiento por id
Route::delete('/products/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (NoExists($id)) {
        abort(404);
    }

    DB::delete('delete from products where id = :id', ['id' => $id]);

    return response()->json('', 200);
});


//Modificar dato por id
Route::put('/products/{id}', function ($id) {
    if (NoExists($id)) {
        abort(404);
    }
    $data = request()->all();
    DB::delete(
        "
        delete from products where id = :id",
        ['id' => $id]
    );
    DB::insert(
        "
        insert into products (id, type, quanty, date, category, name, observations)
        values (:id, :type, :quanty, :date, :category, :name, :observations)
    ",
        $data
    );
    $results = DB::select('select * from products where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});












//funcion de validacion no existe dato
if (!function_exists('NoExists')) {
    function NoExists($id)
    {
        $results = DB::select('select * from products where id=:id', [
            'id' => $id,
        ]);
        return count($results) == 0;
    }
}