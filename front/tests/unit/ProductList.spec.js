import { shallowMount } from '@vue/test-utils'
import ProductList from '@/components/Products/ProductList.vue'
import ProductItem from '@/components/Products/ProductItem.vue'

test('pinta todo list vacío', () => {
    const wrapper = shallowMount(ProductList, {
        propsData: {
            listOfProductz: [],
        },
    })
    const productList = wrapper.findAllComponents(ProductItem).wrappers
    expect(productList.length).toBe(0)
})

test('pinta todo list con product', () => {
    const listOfProductz = [
        {
            id: '1',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '2',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '3',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '4',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
    ]
    const wrapper = shallowMount(ProductList, {
        propsData: {
            listOfProductz: listOfProductz,
        },
    })
    const productList = wrapper.findAllComponents(ProductItem).wrappers
    expect(productList.length).toBe(4)
})

test('emite evento button delete movement', async () => {
    const listOfProductz = [
        {
            id: '1',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '2',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '3',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
        {
            id: '4',
            type: 'entry',
            category: 'papeleria',
            name: 'libro',
            date: '2020-05-27',
        },
    ]
    const wrapper = shallowMount(ProductList, {
        propsData: {
            listOfProductz: listOfProductz,
        },
    })
    wrapper.vm.$emit('delete-product-event', 2)
    /* wrapper.vm.$emit('delete-movement-event', 123)*/
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()['delete-product-event'].length).toBe(1)
    expect(wrapper.emitted()['delete-product-event'][0]).toEqual([2])
})
