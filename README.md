# MyBalanceApp
This project : OSI
INVENTARIO DE SUMINISTROS DE OFICINA , .Office Supply Inventory,
It was designed and created by :
                
                GARCIA Cesar(https://gitlab.com/cesarogarcia/interpenascal)
                
                
Frameworks used : 
                  front -> Vue.js
                  back  -> Laravel
data base used :  SQLite



The tools used in this project are : 
     - Code visual studio : writing and executing code
     - Scrum : project management
     - Gitlab
     - Slack : sharing ideas and documentation.
     
The code using in programation respect certain rules :
* El uso del TDD is obligatory.
*those naming methods should be respected :
    -PascalCase -----> Components
    -kebab-case -----> events , CSS(classes and ID)
    -UPPER_CASE -----> CONSTANTES : names that means the same every time , like (API _ KEY)
    -camelCase  -----> the rest , functions etc
* Use Git for version history. 
* No PUSH without validation of another member.
* Use easily pronounceable names for variables and methods.
* With variables use the name to show intention. 
* Be consistent. 
* Don’t hesitate to use technical terms in names. 
* Use a verb as the first word in method and use a noun for class. Use camelCase for variable and function name. The class should start from the capital.
* Make functions apparent. Keep a function as short as possible.
* Don’t comment on bad code. CHANGE IT
* We vote to result any problem, and if the vote is not woking we ask the project manager. 


For ths new comers to the team, the scrum master or the next person on charge will do the breiving, and have to explain all the steps of the project , and all the procedures.
they should do a git clone, then execute : """npm install """"to have all the packages . 
     """composer install""""  to have all package of laravel en el back 
    """ php artisan key :generate"  en el back  
   edit the archive .env  (en la linea 10) :  
                       DB_CONNECTION=sqlite
                       #DB_CONNECTION=mysql
                       #DB_HOST=127.0.0.1
                       #DB_PORT=3306
                       #DB_DATABASE=laravel
                       #DB_USERNAME=root
                       #DB_PASSWORD=
   Download database.sqlite and past it en the folder database en el folder back. 

