import { shallowMount } from '@vue/test-utils'
import MainPage from '@/components/MainPage.vue'
import {mockRouter} from "./helpers"


function mountPage() {return shallowMount(MainPage, {
    mocks:{$router:mockRouter}
})}


test('pinta el botón go entry', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.go-entry')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón goIncome', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.go-entry').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/inputs')

   
})

test('pinta el botón go-out', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.go-out')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón go-out', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.go-out').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/outputs')

   
})

test('pinta el botón go-products', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.go-product')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón go-products', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.go-product').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/products')

   
})

test('pinta el botón exit-main', () => {
    const wrapper = shallowMount(MainPage, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.exit-main')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón exit-main', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.exit-main').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/home')

   
})