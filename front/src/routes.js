import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/main",
      component: function(resolve) {
        require(["@/components/MainPage.vue"], resolve);
      },
    },

    {
      path: "/home",
      component: function(resolve) {
        require(["@/components/HomePage.vue"], resolve);
      },
    },

    {
      path: "/outputs",
      component: function(resolve) {
        require(["@/components/Outputs/OutPage.vue"], resolve);
      },
    },

    {
      path: "/inputs",
      component: function(resolve) {
        require(["@/components/Inputs/InPage.vue"], resolve);
      },
    },

    {
      path: '/products',
      component: function(resolve) {
          require([
              '@/components/Products/ProductListPage.vue',
          ], resolve)
      },
    },

    {
      path: '/details/:theProductId',
      component: function(resolve) {
          require([
              '@/components/Details/ProductDetailsPage.vue',
          ], resolve)
      },
    },
  ],
});
export default router;
