import { shallowMount } from '@vue/test-utils'
import OutPage from '@/components/Outputs/OutPage.vue'
import OutComponent from '@/components/Outputs/OutComponent.vue'



test('si funciona la function uploadNewOut', async () => {
    const MockUploadNewOut = jest.fn()
    const mockNewOut = [
        {
            id: '89',
            type: 'out',
            quanty: "250",
            date: "2020-04-03",
            category: "COMIDA",
            name: "Cena",
            observations: "cena en familia",

        },
    ]
    MockUploadNewOut.mockReturnValue(mockNewOut)

    const wrapper = shallowMount(OutPage, {
        data() {
            return {}
        },
        methods: {
            ...OutPage.methods,
            uploadNewOut: MockUploadNewOut,
        },
    })
    const newOut = mockNewOut

    await wrapper.vm.$nextTick()

    const Out = wrapper.findComponent(OutComponent)

    Out.vm.$emit('upload-new-out', newOut)

    await wrapper.vm.$nextTick()

    expect(MockUploadNewOut).toHaveBeenCalled()
})
