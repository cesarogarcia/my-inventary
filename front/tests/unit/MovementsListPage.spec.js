import { shallowMount } from '@vue/test-utils'
import ProductListPage from '@/components/Products/ProductListPage.vue'


test('comprobamos que deleteEvent borra el movimiento', async () => {
    const MockApiCall = jest.fn()
    const mockContact = undefined
    MockApiCall.mockReturnValue(mockContact)
    const wrapper = shallowMount(ProductListPage, {
        data() {
            return {
                product: 
                    {
                        id: '1',
                        type: 'entry',
                        category: 'papeleria',
                        name: 'libro',
                        date: '2020-05-27',
                    },
     
            }
        },
        methods: {
            ...ProductListPage.methods,
            deleteProduct: MockApiCall,
            loadProductsList: MockApiCall,
        },
    })
    await wrapper.vm.$nextTick()
    expect(MockApiCall).toHaveBeenCalled()
    expect(wrapper.vm.product.length).toBe(undefined)
})
