import { shallowMount } from '@vue/test-utils'
import OutComponent from '@/components/Outputs/OutComponent.vue'

test('emite event Upload new Out ', async () => {
    const wrapper = shallowMount(OutComponent)
    expect(wrapper.emitted().search).toBe(undefined)

    const date = wrapper.find('.date')
    const quanty = wrapper.find('.quanty')
    const observations = wrapper.find('.observations')
    const submit = wrapper.find('.add-out')

    date.setValue('2020-04-20')
    quanty.setValue('4545')
    observations.setValue('hola')
  

    submit.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["upload-new-out"].length).toBe(1);
    expect(wrapper.emitted()["upload-new-out"]).toEqual([
        [
            {
                
                quanty: '4545',
                observations: 'hola',
                /*type:"out",*/

                category: null,
                date: "2020-04-20",
                namea: null,
                nameb: null,
                namec: null,
                named: null,
                namee: null,

            }
        ]
    ]);
});