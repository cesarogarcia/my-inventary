import { shallowMount } from '@vue/test-utils'
import InPage from '@/components/Inputs/InPage.vue'
import InComponent from '@/components/Inputs/InComponent.vue'



test('si funciona la function uploadNewEntry', async () => {
    const MockUploadNewEntry = jest.fn()
    const mockNewEntry = [
        {
            id: '89',
            type: 'entry',
            quanty: "250",
            date: "2020-04-03",
            category: "COMIDA",
            name: "Cena",
            observations: "cena en familia",

        },
    ]
    MockUploadNewEntry.mockReturnValue(mockNewEntry)

    const wrapper = shallowMount(InPage, {
        data() {
            return {}
        },
        methods: {
            ...InPage.methods,
            uploadNewEntry: MockUploadNewEntry,
        },
    })
    const newEntry = mockNewEntry

    await wrapper.vm.$nextTick()

    const entry = wrapper.findComponent(InComponent)

    entry.vm.$emit('upload-new-entry', newEntry)

    await wrapper.vm.$nextTick()

    expect(MockUploadNewEntry).toHaveBeenCalled()
})
