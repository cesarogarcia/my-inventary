import { shallowMount } from '@vue/test-utils'
import InComponent from '@/components/Inputs/InComponent.vue'

test('emite event Upload new Out ', async () => {
    const wrapper = shallowMount(InComponent)
    expect(wrapper.emitted().search).toBe(undefined)

    const date = wrapper.find('.date')
    const quanty = wrapper.find('.quanty')
    const observations = wrapper.find('.observations')
    const submit = wrapper.find('.add-entry')

    date.setValue('2020-04-20')
    quanty.setValue('4545')
    observations.setValue('hola')
  

    submit.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["upload-new-entry"].length).toBe(1);
    expect(wrapper.emitted()["upload-new-entry"]).toEqual([
        [
            {
                
                quanty: '4545',
                observations: 'hola',
                /*type:"entry",*/

                category: null,
                date: "2020-04-20",
                namea: null,
                nameb: null,
                namec: null,
                named: null,
                namee: null,

            }
        ]
    ]);
});