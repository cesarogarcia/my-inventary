import { shallowMount } from "@vue/test-utils";
import ProductDetails from "@/components/Details/ProductDetails.vue";
test("Comprobamos que información de App imprimimos en la pantalla", () => {
  //crear componente con las propiedades
  //comprobar que el nombre y el apellido estan en su sitio
  const wrapper = shallowMount(ProductDetails, {
    propsData: {
        productDetail: {
        name: "cesar",
        type: "out",
        quanty: "16",
        date:"2020-06-16",
        category: "Papeleria",
        observations: "hola",
        dateaudit:"2020-10-16"


      },stock:null
      
    }
  });
  //buscamos la clase de la plantilla html (find)
  /*const stockTemplate = wrapper.find(".span-stock");*/
  const nameTemplate = wrapper.find(".span-name");
  const typeTemplate = wrapper.find(".span-type");
  const quantyTemplate = wrapper.find(".span-quanty");
  const dateTemplate = wrapper.find(".span-date");
  const categoryTemplate = wrapper.find(".span-category");
  const observationsTemplate = wrapper.find(".span-observations");
  const dateAuditTemplate = wrapper.find(".span-dateaudit");

  /*const textStock = stockTemplate.text();*/
  const textName = nameTemplate.text();
  const textType = typeTemplate.text();
  const textQuanty = quantyTemplate.text();
  const textDate = dateTemplate.text();
  const textCategory = categoryTemplate.text();
  const textObservations = observationsTemplate.text();
  const textDateAudit = dateAuditTemplate.text();

  /*expect(textStock).toBe("");*/
  expect(textName).toBe("Producto :cesar");
  expect(textType).toBe("Tipo :out");
  expect(textQuanty).toBe("Cantidad Registrada :16");
  expect(textDate).toBe("Fecha :2020-06-16");
  expect(textCategory).toBe("Categoria :Papeleria");
  expect(textObservations).toBe("Observaciones: hola");
  expect(textDateAudit).toBe("Fecha real de gestión :2020-10-16");
});
