import { shallowMount } from '@vue/test-utils'
import SearchNameComponent from '@/components/Products/SearchNameComponent.vue'
import { mockRouter } from './helpers'

function mountPage() {
    return shallowMount(SearchNameComponent, {
        mocks: { $router: mockRouter },
    })
}

test('emite evento click buttonSearch', async () => {
    const wrapper = shallowMount(SearchNameComponent)
    expect(wrapper.emitted('button-search-event')).toBe(undefined)
    const inputSearch = wrapper.find('.search-product')
    const search = wrapper.find('.search-product')

    // Simular lo que hace el usuario.

    inputSearch.setValue('Sacapuntas')
    search.trigger('click')
    await wrapper.vm.$nextTick()

    // Comprobar la emisión.
    expect(wrapper.emitted('button-search-event').length).toBe(1)
    expect(wrapper.emitted('button-search-event')).toEqual([
        ['Sacapuntas'],
    ])
})

test('pinta el botón buttonBack', () => {
    const wrapper = shallowMount(SearchNameComponent, {
        propsData: {
            state: true,
        },
    })
    let button = wrapper.find('.button-back')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el botón buttonBack', async () => {
    const wrapper = mountPage()
    wrapper.findAll('.button-back').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.$router.path).toBe('/main')
})
